<?php

/**
 * Implements hook_preprocess_page().
 */
function cihr_irsc_preprocess_page(&$variables) {
  // You can use preprocess hooks to modify the variables before they are passed
  // to the theme function or template file.
  global $language;

  $variables['wmms'] = drupal_get_path('theme', 'cihr_irsc') . '/libraries/cihr-irsc/wmms-intra.svg';
  $variables['wmms_alt'] = drupal_get_path('theme', 'cihr_irsc') . '/libraries/cihr-irsc/wmms.png';

  if ($language->language == 'en') {
    $variables['logo_svg'] = drupal_get_path('theme', 'cihr_irsc') . '/libraries/cihr-irsc/cihr-logo-en.svg';
    $variables['logo'] = drupal_get_path('theme', 'cihr_irsc') . '/libraries/cihr-irsc/cihr-logo-en.png';
  }
  if ($language->language == 'fr') {
    $variables['logo_svg'] = drupal_get_path('theme', 'cihr_irsc') . '/libraries/cihr-irsc/cihr-logo-fr.svg';
    $variables['logo'] = drupal_get_path('theme', 'cihr_irsc') . '/libraries/cihr-irsc/cihr-logo-fr.png';
  }
  $variables['wetkit_sub_site'] = '';
}